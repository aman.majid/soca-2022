'''

utils_benchmarking.py

    author: @amanmajid

'''

import datatoolbox as dt
from .global_variables import *

def calculate_historical_generational_share(technology='renewable',energy_source='IEA_WEB_2019'):
    '''Calculate the historic generational share of a given technology
    '''
    #%% Renewable share - Historic data
    if technology == 'renewable':
        techOutput = dt.getTable(f'{iea_historic_renewable_generation}__{energy_source}')
    elif technology == 'coal':
        techOutput = dt.getTable(f'{iea_historic_coal_generation}__{energy_source}')
    totalOutput = dt.getTable(f'{iea_historic_total_generation}__{energy_source}')
    tech_share_historic = techOutput / totalOutput   
    tech_share_historic = tech_share_historic.convert('%')
    return tech_share_historic


def calculate_benchmarks(technology,generation_types,source='IPCC_SR15'):
    '''Function to get 1.5 degree compatible renewable generation shares from SR1.5 results
    '''
    gen_share_data = dt.TableSet()
    for pathway in compatible_pathways: 

        renewables_total = sum([dt.getTable('__'.join([x, pathway, source])) for x in generation_types])


        total = dt.getTable('__'.join(['Secondary Energy|Electricity', pathway, source]))
        
        re_share = (renewables_total / total).convert('%')

        re_share.meta.update({'entity' : f'{technology}_share',
                              'category' : 'Secondary Energy|Electricity',
                              'pathway' : pathway,
                              'source' : 'CAT_benchmarks_2022'})

        gen_share_data[pathway] = re_share
        return gen_share_data