'''

global_variables.py

    author: @amanmajid

'''

# IEA data variables
iea_historic_renewable_generation = 'Electricity|generation|Renewables__Historic'
iea_historic_coal_generation      = 'Electricity|generation|Coal__Historic'
iea_historic_total_generation     = 'Electricity|generation|Total__Historic'


# 1.5 degree compatible pathways
compatible_pathways = [
    'IMA15-LiStCh|IMAGE 3.0.1',
    'CD-LINKS_NPi2020_1000|WITCH-GLOBIOM 4.4',
    'CD-LINKS_NPi2020_400|WITCH-GLOBIOM 4.4',
    'ADVANCE_2020_1.5C-2100|MESSAGE-GLOBIOM 1.0',
    'SSP1-19|AIM_CGE 2.0',
    'SSP2-19|AIM_CGE 2.0',
    'TERL_15D_LowCarbonTransportPolicy|AIM_CGE 2.1',
    'TERL_15D_NoTransportPolicy|AIM_CGE 2.1',
]


# generations types to sum for renewables
renewables_generation_types = [
    'Secondary Energy|Electricity|Biomass',
    'Secondary Energy|Electricity|Hydro',
    # 'Secondary Energy|Electricity|Nuclear',
    'Secondary Energy|Electricity|Solar',
    'Secondary Energy|Electricity|Wind'
]


# generations types to sum for UNABATED coal
unabated_coal_generation_types = [
    'Secondary Energy|Electricity|Coal|w_o CCS'
]